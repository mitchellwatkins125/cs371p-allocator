# CS371p: Object-Oriented Programming Allocator Repo

* Name: (Mitchell Watkins, Adam Gluch)

* EID: (maw5236, amg6796)

* GitLab ID: (mitchellwatkins125, amgluch)

* HackerRank ID: (adammgluch)

* Git SHA: (7c8a89df1b2675c6522545200226622c8230c56d)

* GitLab Pipelines: (https://gitlab.com/mitchellwatkins125/cs371p-allocator/-/pipelines)

* Estimated completion time: (12)

* Actual completion time: (16)

* Comments: ()
