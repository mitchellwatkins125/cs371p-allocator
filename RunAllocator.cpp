// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream> // cin, cout
#include <stdio.h>
#include <string>
#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
using namespace std;


template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return lhs._p == rhs._p;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        int* operator * () const {
            return _p;
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            //Read what sentinental value we are on, and move accordingly.
            //1000: read 9992 ->


            int sentinel_val = *_p;
            if (sentinel_val < 0)
                sentinel_val *= -1;


            char* cursor = (char*) _p;
            cursor += 2 * sizeof(int) + sentinel_val;
            //Starting point + 2 sentinels + allocated bytes

            _p = (int*) cursor;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () {
            //Read what sentinental value we are on, and move accordingly.
            //1000: read 9992 ->

            int* pcopy = _p;
            int sentinel_val = *--pcopy;

            if (sentinel_val < 0)
                sentinel_val *= -1;

            char* cursor = (char*) _p;
            cursor -= (2 * sizeof(int) + sentinel_val);
            //Starting point + 2 sentinels + allocated bytes

            _p = (int*) cursor;
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return lhs._p == rhs._p;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            static int tmp = _p;
            return tmp;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {
            int sentinel_val = *_p;
            if (sentinel_val < 0)
                sentinel_val *= -1;


            char* cursor = (char*) _p;
            cursor += 2 * sizeof(int) + sentinel_val;
            //Starting point + 2 sentinels + allocated bytes

            _p = (const int*) cursor;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {
            int* pcopy = _p--;
            int sentinel_val = *pcopy;
            if (sentinel_val < 0)
                sentinel_val *= -1;

            char* cursor = (char*) _p;
            cursor -= 2 * sizeof(int) + sentinel_val;
            //Starting point + 2 sentinels + allocated bytes

            _p = (const int*) cursor;
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * <your documentation>
     */
    bool valid () const {
        // const_iterator b = begin();
        // const_iterator e = end();

        // while (b != e){
        //     if (*b < (sizeof(T) + 2 * sizeof(int))){
        //         return
        //     }
        // }

        return true;
    }

public:
    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        if (N < sizeof(T) + (2 * sizeof(int)))
            throw std::bad_alloc();

        //Set up first free block with its sentinels

        int sentinel_val = N - 2 * sizeof(int);
        (*this)[0] = sentinel_val; // sentinel 1
        (*this)[sentinel_val + sizeof(int)] = sentinel_val; //sentinel 2
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------
    void print_allocator() {
        iterator b = begin();
        iterator e = end();
        while (b != e) {
            int* sentinel1 = *b;
            printf("%d", *sentinel1);
            ++b;
            if (b != e) {
                printf(" ");
            }
        }
    }
    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type n) {

        //Find first free block that can take n * size(T)
        iterator b = begin();
        iterator e = end();


        while (b != e) {
            int* sentinel1 = *b;
            char* cursor = (char*) sentinel1;

            int spaceOfBlock = *sentinel1;
            int bytesToAllocate = n * sizeof(T);

            //This is too big, can't allocate in this block, try the next one
            if (bytesToAllocate > spaceOfBlock) {
                ++b;
                continue;
            }

            //If we are allocating and it takes up the free block perfectly
            if (bytesToAllocate == spaceOfBlock) {
                *sentinel1 = -1 * spaceOfBlock;
                cursor += spaceOfBlock + sizeof(int);
                int* sentinel2 = (int*) cursor;
                *sentinel2 = -1 * spaceOfBlock;
                return (pointer) ++sentinel1;
            }

            int margin = sizeof(T) + (2 * sizeof(int));
            int spaceNeeded = bytesToAllocate + (2 * sizeof(int));

            int freeSpaceAfterSplit = spaceOfBlock + (2 * sizeof(int)) - spaceNeeded;

            //If we can't split without keeping the margin free size, allocate the whole free block
            if (freeSpaceAfterSplit < margin) {
                *sentinel1 = -1 * spaceOfBlock;
                cursor += spaceOfBlock + sizeof(int);
                int* sentinel2 = (int*) cursor;
                *sentinel2 = -1 * spaceOfBlock;
                return (pointer) ++sentinel1;
            }

            //split the free space!
            if (spaceOfBlock + (2 * sizeof(int)) >= spaceNeeded + margin) {
                int usedSentinelVal = -1 * bytesToAllocate;                 //value of usedsentinel
                int newFreeSentinelVal = spaceOfBlock - spaceNeeded;        //value of free sentinel
                *sentinel1 = usedSentinelVal;                               //Write first usedsentinel
                cursor += (-1 * usedSentinelVal) + sizeof(int);             //move cursor for one sentinel and allocated bytes
                int* sentinel2 = (int*) cursor;                             //cast cursor to  int* so we can write in a new sentinel
                *sentinel2 = usedSentinelVal;                               //write in second used
                sentinel2 += 1;                                             //move 4 bytes to prepare to write free sentinel
                *sentinel2 = newFreeSentinelVal;                            //write in first free sentinel
                cursor = (char*) sentinel2;                                 //convert to char* so we can move freely
                cursor += newFreeSentinelVal + sizeof(int);                 //move cursor for one sentinel and amount of free bytes
                sentinel2 = (int*) cursor;                                  //convert cursor to int* so we can write in sentinel
                *sentinel2 = newFreeSentinelVal;                            //write in free sentinel
                return (pointer) ++sentinel1;
            }
            assert(false);
            ++b;
        }
        throw std::bad_alloc();
        assert(valid());
        return nullptr;
    }             // replace!

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * <your documentation>
     */
    void deallocate (pointer p) {
        int* sentinel = (int*) p;
        --sentinel;
        int sentinelValue = *sentinel;
        if (sentinelValue < 0) {
            sentinelValue *= -1;
        }
        // if ( (sentinelValue / sizeof(value_type) ) < s) {
        //     throw std::invalid_argument("");
        // }

        iterator b = begin();
        iterator e = end();

        iterator it = iterator(sentinel);

        iterator leftNeighbor = NULL;
        iterator rightNeighbor = NULL;


        leftNeighbor = it != b ? --it : NULL;
        it = iterator(sentinel);
        rightNeighbor = ++it != e ? it : NULL;

        bool leftAllocated = false;
        bool rightAllocated = false;



        leftAllocated = leftNeighbor != NULL ? ((**leftNeighbor) < 0) : true;
        rightAllocated = rightNeighbor != NULL ? ((**rightNeighbor) < 0) : true;

        // printf("\n**********************left Allocated = %d\n", leftAllocated);
        // if (!leftAllocated)
        //     printf("\n********************left neighbor = %d\n\n", **leftNeighbor);

        // printf("\n**********************right Allocated = %d\n", rightAllocated);
        // if (!rightAllocated)
        //     printf("\n********************right neighbor = %d\n", **rightNeighbor);

        //case1:     allocated/edge - *here* - allocated/edge
        //case2:        free   - *here* -  allocated/edge
        //case3:     free       - *here* -  free
        //case4:      allocated/edge  - *here  -  free
        char* cursor;
        it = iterator(sentinel);
        // printf("**********************IT before loop = %d\n", **it);
        if ( leftAllocated && rightAllocated) {
            int* sentinel1 = *it;                           //Address of first sentinel
            // printf("ABOUT TO FLIP FIRST SENTINEL OF VALUE %d\n", *sentinel1);
            *sentinel1 *= -1;                               //Free it by *=1
            int sentinelValue = *sentinel1;                 //storing the sentinel value
            cursor = (char*) sentinel1;             //Make a cursor
            cursor += sentinelValue + (sizeof(int));         //Move sentinel value + size of sentinel
            sentinel1 = (int*) cursor;                      //free the second sentinel
            // printf("ABOUT TO FLIP SECOND SENTINEL OF VALUE %d\n", *sentinel1);
            *sentinel1 *= -1;
        } else if (!leftAllocated && rightAllocated) {
            int* leftFreeSentinel = *leftNeighbor;
            int* currentSentinel = *it;
            int newBlockSize = *leftFreeSentinel + ( (*currentSentinel) * -1) + (2 * sizeof(int));
            *leftFreeSentinel = newBlockSize;
            cursor = (char*) leftFreeSentinel;
            cursor += newBlockSize + sizeof(int);
            leftFreeSentinel = (int*) cursor;
            *leftFreeSentinel = newBlockSize;
        }
        else if (leftAllocated && !rightAllocated) {
            int* rightFreeSentinel = *rightNeighbor;
            int* currentSentinel = *it;
            int newBlockSize = *rightFreeSentinel + ( (*currentSentinel) * -1) + (2 * (sizeof(int)));
            *currentSentinel = newBlockSize;
            cursor = (char*) currentSentinel;
            cursor += newBlockSize + sizeof(int);
            rightFreeSentinel = (int*) cursor;
            *rightFreeSentinel = newBlockSize;
        }
        else { //neither
            int* leftFreeSentinel = *leftNeighbor;
            int* currentSentinel = *it;
            int* rightFreeSentinel = *rightNeighbor;
            int newBlockSize = *leftFreeSentinel + ( (*currentSentinel) * -1) + *rightFreeSentinel + (4 * sizeof(int));
            *leftFreeSentinel = newBlockSize;
            cursor = (char*) leftFreeSentinel;
            cursor += newBlockSize + sizeof(int);
            leftFreeSentinel = (int*) cursor;
            *leftFreeSentinel = newBlockSize;
        }
        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }


    double* find_busy_block(int bb_num) {
        iterator b = begin();
        iterator e = end();
        int counter = 0;
        while (b != e) {
            if(**b < 0)
                counter--;
            if(counter == bb_num) {
                char* cursor = (char*) *b;
                cursor += 4;
                int* result = (int*) cursor;
                return (double*) result;
            }
            ++b;
        }
        return nullptr;
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(&(*this)[0]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return const_iterator(&(*this)[0]);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        return iterator(&(*this)[N]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return const_iterator(&(*this)[N]);
    }
};

bool is_int(string s) {
    if (s.length() < 1) {
        return false;
    }

    for (int i = 0; i < s.length(); ++i) {
        if (!isdigit(s.at(i))) {
            return false;
        }
    }
    return true;
}


int get_num_lines(istream& sin, string s) {
    bool eof = !getline(sin, s);
    if (eof) return -1;
    int num_lines = stoi(s);
    assert(num_lines > 0 && num_lines <= 1000);
    return num_lines;
}

void assert_num_cases(istream& sin) {
    string s;
    bool eof;
    eof = !getline(sin, s);
    assert(!eof);
    assert(is_int(s));
    int num_cases = stoi(s);
    assert(num_cases > 0);
    assert(num_cases <= 100);
}



int solve(istream& sin, ostream& sout) {
    string s;
    assert_num_cases(sin);

    // Clear initial white space
    bool eof = !getline(sin, s);
    getline(sin, s);
    assert(!eof);
    //we are past number of tests and the blank line that comes after
    while (!eof) {
        //construct new allocator
        my_allocator<double, 1000> allocator = my_allocator<double, 1000>();
        while(s.length() != 0) { //do allocator stuff
            int command = std::stoi(s);
            if (command < 0) {
                double* rp = allocator.find_busy_block(command);
                int* ip = (int*) rp;
                allocator.deallocate(rp);
            }
            else
                allocator.allocate(command);
            getline(sin, s);
        } //end of test case, next iteration new allocator
        allocator.print_allocator();
        printf("\n");
        eof = !getline(sin, s);
    }
    return 0;
}


int main () {
    solve(cin, cout);
    return 0;
}
